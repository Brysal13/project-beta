# CarCar

Team:

* Zach Winter - Sales
* Bryant Salazar - Service

---
## How to Run

Fork project from Gitlab repository

Open up terminal

CD into a project file or any files of your choosing.

Clone the file from Gitlab, use the following terminal commands replacing cloned url with the actual url:

```
git clone <<<cloned url>>>
cd project-beta
```

 Open Docker desktop to make sure that the containers properly run once started. If this your first time running the app, it may take a few minutes to start up. Use the following commands to start the containers:
 ```
    docker volume create beta-data
    docker-compose build
    docker-compose up
```

Once all containers are up and running, open url:

http://localhost:3000

---
## Diagram

![Alt text](ghi/app/public/CarCar.png)

---
## Sales microservice

## API Documentation:

The Sales API has fully functional RESTful endpoints for the following entities:

    Salesperson: An employee who is selling the car.

    Customer: A customer who is buying the car.

    Sale: A record of the sale.


<br>

### Sales People
| Action             | Method | URL                                       |
| ------             | ------ |------                                     |
| List salespeople   | GET    | http://localhost:8090/api/salespeople/    |
| Create salesperson | POST   | http://localhost:8090/api/salespeople/    |
| Delete salesperson | DELETE | http://localhost:8090/api/salespeople/:id |

<br>

"GET" request returns:

```
{
	"salespeople": [
		{
			"href": "/api/salespeople/1/",
			"id": 1,
			"first_name": "Zach",
			"last_name": "Winter",
			"employee_id": "Zwinter"
		},
        ...
    ]
}
```

<br>

"POST" request needs this json data to be successful:
```
{
    "first_name": "Zachary",
	"last_name": "Winters",
	"employee_id": "ZZwinter"
}
```
and this header: Content-Type | application/json

Returns:
```
{
	"href": "/api/salespeople/1/",
	"id": 1,
	"first_name": "Zachary",
	"last_name": "Winters",
	"employee_id": "ZZwinter"
}
```
<br>

"DELETE" request needs the id of the Sales person you want to delete at the end of the url:

http://localhost:8090/api/salespeople/1

Returns:
```
{
	"message": "Salesperson has been deleted."
}
```
<br>

---

<br>

### Customers
| Action           | Method | URL                                     |
| ------           | ------ |------                                   |
| List customers   | GET    | http://localhost:8090/api/customers/    |
| Create customer  | POST   | http://localhost:8090/api/customers/    |
| Delete customer  | DELETE | http://localhost:8090/api/customers/:id |

<br>

"GET" request returns:

```
{
	"customers": [
		{
			"id": 1,
			"first_name": "Zach",
			"last_name": "Winter",
			"address": "2345 street, Town, City",
			"phone_number": "806-345-2364"
		},
        ...
    ]
}
```

<br>

"POST" request needs this json data to be successful:
```
{
    "first_name": "Zachary",
	"last_name": "Winter",
	"address": "2345 street, Town, City",
	"phone_number":"806-345-2364"
}
```
and this header: Content-Type | application/json

Returns:
```
{
	"id": 5,
	"first_name": "Zachary",
	"last_name": "Winter",
	"address": "2345 street, Town, City",
	"phone_number": "806-345-2364"
}
```
<br>

"DELETE" request needs the id of the Customer you want to delete at the end of the url:

http://localhost:8090/api/customers/5

Returns:
```
{
	"message": "Customer has been deleted."
}
```

<br>

---

<br>

### Sales
| Action      | Method | URL                                 |
| ------      | ------ |------                               |
| List sales  | GET    | http://localhost:8090/api/sales/    |
| Create sale | POST   | http://localhost:8090/api/sales/    |
| Delete sale | DELETE | http://localhost:8090/api/sales/:id |

<br>

"GET" request returns:

```
{
	"sales": [
		{
			"id": 1,
			"automobile": {
				"vin": "1C3CC5FB2AN120174"
			},
			"salesperson": {
				"href": "/api/salespeople/1/",
				"id": 1,
				"first_name": "Zach",
				"last_name": "Winter",
				"employee_id": "Zwinter"
			},
			"customer": {
				"id": 1,
				"first_name": "Zach",
				"last_name": "Winter",
				"address": "2345 street, Town, City",
				"phone_number": "1234567891"
			},
			"price": "6000.00"
		},
        ...
    ]
}
```

<br>

"POST" request needs this json data to be successful:
```
{
    "automobile": "1C3CC5FB2AN120175",
	"salesperson": 4,
	"customer": 4,
	"price": 15000
}
```
and this header: Content-Type | application/json

Returns:
```
{
	"id": 7,
	"automobile": {
		"vin": "1C3CC5FB2AN120175"
	},
	"salesperson": {
		"href": "/api/salespeople/4/",
		"id": 4,
		"first_name": "Mitch",
		"last_name": "Drift",
		"employee_id": "MDrift"
	},
	"customer": {
		"id": 4,
		"first_name": "Irene",
		"last_name": "Stagmire",
		"address": "2344 Stillberry rd",
		"phone_number": "806-434-5433"
	},
	"price": 15000
}
```
<br>

"DELETE" request needs the id of the Sale you want to delete at the end of the url:

http://localhost:8090/api/sales/7

Returns:
```
{
	"message": "Sale has been deleted."
}
```
<br>

---

### Sales API Value Objects

The only VO is the AutomobileVO which stores the VIN value and is retrieved from the Inventory API through the poller.py.

<br>

---

## Inventory microservice

## API Documentation:

The Inventory API has fully functional RESTful endpoints for the following entities:

    Manufacturer: the company that manufactures the automobile

    VehicleModel: the model of a vehicle created by the manufacturer

    Automobile: the actual automobile of a specific vehicle model

<br>

### Manufacturer
| Action               | Method | URL                                          |
| ------               | ------ |------                                        |
| List manufacturers   | GET    | http://localhost:8100/api/manufacturers/     |
| Create manufacturer  | POST   | http://localhost:8100/api/manufacturers/     |

<br>

"GET" request returns:

```
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    },
    ...
  ]
}
```

<br>

"POST" request needs this json data to be successful:
```
{
  "name": "Chrysler"
}
```
and this header: Content-Type | application/json

Returns:
```
{
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Chrysler"
}
```

---

<br>

### Vehicle Models
| Action                | Method | URL                                     |
| ------                | ------ |------                                   |
| List Vehicle Models   | GET    | http://localhost:8100/api/models/    |
| Create Vehicle Models | POST   | http://localhost:8100/api/models/    |


<br>

"GET" request returns:

```
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}
```

<br>

"POST" request needs this json data to be successful:
```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
```
and this header: Content-Type | application/json

Returns:
```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}
```

<br>

---

<br>

### Automobile
| Action      | Method | URL                                 |
| ------      | ------ |------                               |
| List Automobiles   | GET    | http://localhost:8100/api/automobiles/    |
| Create Automobiles | POST   | http://localhost:8100/api/automobiles/    |

<br>

"GET" request returns:

```
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      },
      "sold": false
    }
    ...
  ]
}
```

<br>

"POST" request needs this json data to be successful:
```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```
and this header: Content-Type | application/json

Returns:
```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "yellow",
  "year": 2013,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  },
  "sold": false
}
```


## Service microservice


The Service API has fully functional RESTful endpoints for the following entities:

    Technician : An employee that's in charge of working on automobiles.

    Appointments : Service appointments for the vehicles to be worked on.

    Appointment Status: It shows whether a vehicle is finished being serviced or if appointment was canceled.

<br>

### Technicians
| Action             | Method | URL                                       |
| ------             | ------ |------                                     |
| List technicians  | GET    | http://localhost:8080/api/technicians/    |
| Create technicians | POST   | http://localhost:8080/api/technicians/    |
| Delete technicians | DELETE | http://localhost:8080/api/technicians/:id |

<br>

"GET" request returns:

```
{
    "technicians": [
        {
            "id": 1,
            "name": "Bryant Salazar",
            "employee_number": "bsalazar"
        },
        ...
    ]
}
```

<br>

"POST" request needs this json data to be successful:
```
{
    "name": "Bryant Salazar",
    "employee_id": "Bsalazar"
}
```
and this header: Content-Type | application/json

Returns:
```
{
    "href": "/api/technicians/1/",
    "id": 1,
    "name": "Bryant Salazar"
    "employee_number": "bsalazar"
}
```
<br>

"DELETE" request needs the id of the Technician person you want to delete at the end of the url:

http://localhost:8080/api/technicians/1

Returns:
```
{
    "message": "Technician has been deleted."
}
```
<br>

### Appointments
| Action           | Method | URL                                     |
| ------           | ------ |------                                   |
| List appointments   | GET    | http://localhost:8080/api/appointments/    |
| Create appointments  | POST   | http://localhost:8080/api/appointments/    |
| Delete appointments  | DELETE | http://localhost:8080/api/appointments/:id |

<br>

"GET" request returns:

```
{
   "appointments": [
        {
            "id": 1,
			"vin_num": "WDDNG7GB8BA416012",
			"owner": "Bryant Salazar",
			"date": "2023-04-28",
			"time": "15:00:00",
			"reason": "Oil change",
			"technician": "Bryant Salazar",
			"is_vip": false,
			"is_finished": false
        },
        ...
   ]
}
```

<br>

"POST" request needs this json data to be successful:
```
{
    "vin_num": "WDDNG7GB8BA416012",
    "owner": "Bryant Salazar",
    "date": 2023-04-28,
    "time": 15:00:00,
    "reason":"Oil change",
    "technician": "use the technician ID number"
}
```
and this header: Content-Type | application/json

Returns:
```
{
	"id": 8,
	"vin_num": "WDDNG7GB8BA416012",
	"owner": "Bryant Salazar",
	"date": "2023-04-28",
	"time": "15:30:00",
	"reason": "Oil change",
	"technician": "Bryant Salazar",
	"is_vip": false,
	"is_finished": false
}
```
<br>

"DELETE" request needs the id of the Technician you want to delete at the end of the url:

http://localhost:8080/api/technicians/7

Returns:
```
{
    "message": "Technician has been deleted."
}
```

<br>

---

### Service API Value Objects

The only VO is the Automobile VO. That stores the VIN value and gets retrieved from the Inventory API through poller.py
