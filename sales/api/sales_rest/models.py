from django.db import models
from django.urls import reverse

# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.vin

class SalesPerson (models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200,)

    def __str__(self):
        return self.employee_id

    def get_api_url(self):
        return reverse("api_show_salespeople", kwargs={"id": self.id})


class Customer (models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=12)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_customers", kwargs={"id": self.id})

class Sale (models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sale",
        on_delete=models.PROTECT,
        )

    salesperson = models.ForeignKey(
        SalesPerson,
        related_name="sale",
        on_delete=models.CASCADE,
        )

    customer = models.ForeignKey(
        Customer,
        related_name="sale",
        on_delete=models.PROTECT,
        )

    price = models.DecimalField(max_digits=10 , decimal_places=2)

    def __str__(self):
        return self.salesperson.employee_id

    def get_api_url(self):
        return reverse("api_show_sales", kwargs={"id": self.id})
