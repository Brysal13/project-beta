import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelList from './VehicleModelList';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';
import VehicleModelForm from './VehicleModelForm';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import VinHistory from './VinHistory';
import SalesPeopleForm from './SalesPeopleForm';
import SalesPeopleList from './SalesPeopleList';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import SalesPeopleHistoryList from './SalespersonHistory';

import TechnicianList from './TechnicianList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="models/">
          <Route path="" element={<VehicleModelList />} />
          <Route path="new/" element={<VehicleModelForm />} />
          </Route>

          <Route path="automobiles/">
          <Route path="new/" element={<AutomobileForm />} />
          <Route path="" element={<AutomobileList />} />
          </Route>

          <Route path="salespeople/">
          <Route path="" element= { <SalesPeopleList/>}/>
          <Route path="new/" element= { <SalesPeopleForm/>}/>
          </Route>

          <Route path="customers/">
          <Route path="" element= { <CustomerList/>}/>
          <Route path="new/" element= { <CustomerForm/>}/>
          </Route>

          <Route path="manufacturers/">
          <Route path="" element= { <ManufacturersList/> }/>
          <Route path="new/" element= { <ManufacturerForm/> }/>
          </Route>

          <Route path="sales/">
          <Route path="" element= { <SalesList/> }/>
          <Route path="new/" element= { <SalesForm/> }/>
          <Route path="history/" element= { <SalesPeopleHistoryList/> }/>
          </Route>

          <Route path="technicians/new/" element={<TechnicianForm />} />
          <Route path="technicians/" element={<TechnicianList />} />
          <Route path="service-appointments/">
          <Route path="" element={<AppointmentList />} />
          <Route path="new" element={<AppointmentForm />} />
          <Route path="history" element={<VinHistory />} />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
