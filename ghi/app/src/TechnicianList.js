import React from 'react';

class TechnicianList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            technicians: []
        };
    };

    async componentDidMount() {
        const URL = 'http://localhost:8080/api/technicians/';
        const response = await fetch(URL);
        if (response.ok) {
            const data = await response.json();
            this.setState({ technicians: data.technicians})
        }
    }
    render(){
        return (
            <table className='table table-striped mt-5'>
                <thead>
                    <tr>
                        <th>Technician Name</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.technicians.map(technicians => {
                        return(
                            <tr key={technicians.id}>
                                <td>{technicians.name}</td>
                                <td>{technicians.employee_number}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        )
    }
}
export default TechnicianList;
