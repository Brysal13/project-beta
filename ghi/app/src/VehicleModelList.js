import { useEffect, useState } from 'react';


function VehicleModelList() {

    // Set Vehicle Model State
    const [models, setModels] = useState([]);
    async function getModels() {
        const url = "http://localhost:8100/api/models/"
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }

    useEffect(() => {
        getModels();
    },[]);

    return (
        <table  className='table table-striped'>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
            {models.map((model) => {
                return(
                    <tr key={model.id}>
                        <td>{model.name}</td>
                        <td>{model.manufacturer.name}</td>
                        <td><img src={model.picture_url} alt='CarImg'/></td>
                    </tr>
                );
            })}
            </tbody>
        </table>
    );
}

export default VehicleModelList;
