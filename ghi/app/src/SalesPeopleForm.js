import { useState} from 'react';
import { useNavigate } from 'react-router-dom';

function SalesPeopleForm() {
    const[ first_name, setFirstName ] = useState("");
    const[ last_name, setLastName ] = useState("");
    const[ employee_id, setEmployeeId ] = useState("");
    const navigate = useNavigate();

    // HANDLE SUBMIT EVENTS
    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            first_name,
            last_name,
            employee_id,
        };
        const url = "http://localhost:8090/api/salespeople/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url,fetchConfig);
        if (response.ok) {
            const newSalesPerson = await response.json();
            setFirstName("");
            setLastName("");
            setEmployeeId("");
            navigate("/salespeople");
        }
    }

    // HANDLE CHANGE EVENTS
    function handleFirstNameChange(event) {
        const value = event.target.value;
        setFirstName(value);
    }

    function handleLastNameChange(event) {
        const value = event.target.value;
        setLastName(value);
    }

    function handleEmployeeIdChange(event) {
        const value = event.target.value;
        setEmployeeId(value);
    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Add a New Salesperson</h1>
            <form onSubmit={handleSubmit} id="create-Salespeople-form">
                <div className="form-floating mb-3">
                <input onChange={handleFirstNameChange} value={first_name} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                <label htmlFor="first_name">First Name</label>
                </div>

                <div className="form-floating mb-3">
                <input onChange={handleLastNameChange} value={last_name} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                <label htmlFor="last_name">Last Name</label>
                </div>

                <div className="form-floating mb-3">
                <input onChange={handleEmployeeIdChange} value={employee_id} placeholder="Employee Id" required type="text" name="employee_id" id="employee_id" className="form-control" />
                <label htmlFor="employee_id">Employee Id</label>
                </div>

                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    );
}

export default SalesPeopleForm;
